
from numpy import asarray
from utils import *

imgs_path = '/home/mrrevillod/peter/proyecto1-interfaces/Interfaces/1/imgs/'
saves_path = '/home/mrrevillod/peter/proyecto1-interfaces/Interfaces/1/saves/'

# imgs_path = '/app/imgs/'
# saves_path = '/app/saves/'

def uno_a(I: Image):
    print('Calculos de la figura 1a: ')
    src = asarray(I)
    new_src = to_binary_region1(src)

    x = centroide(new_src)

    marca_centroide(I, x)
    I.save(saves_path + 'new_1a.png')

    print('  Área: ', m(new_src, 0, 0))
    print('  El centroide de la imagen esta en las coordenadas: ', x)
    print('  El centroide fue marcado y guardado en la imagen :',
          saves_path + 'new_1a.png')


def uno_b(I: Image):
    print('Calculos de la figura 1b: ')
    src = asarray(I)
    new_src = to_binary_region1(src)

    m23 = m(new_src, 2, 3)
    u23 = u(new_src, 2, 3)
    u_n23 = u_n(new_src, 2, 3)

    print('  El momento de orden p = 2 y q = 3:', m23)
    print('  El momento central de orden p = 2 y q = 3:', u23)
    print('  El momento central normalizado de orden p = 2 y q = 3:', u_n23)


def uno_c(I: Image):
    print('Calculos de la figura 1c: ')
    src = asarray(I)
    new_src = to_binary_region1(src)

    hu1 = Hu_1(new_src)
    hu2 = Hu_2(new_src)
    hu3 = Hu_3(new_src)

    print('  Momento de Hu 1:', hu1)
    print('  Momento de Hu 2:', hu2)
    print('  Momento de Hu 3:', hu3)


def dos(I: Image):
    print('Calculos de la figura 2:')
    src = asarray(I)
    hist = get_hist(src)

    fig, ax = plt.subplots()
    x = np.arange(0, 256)

    ax.plot(x, hist[0], color='r', label='histogram red')
    ax.plot(x, hist[1], color='g', label='histogram green')
    ax.plot(x, hist[2], color='b', label='histogram blue')

    plt.xlabel('Intensidad')

    fig.legend()


def tres(I: Image):
    print('Calculos de la figura 3:')
    src = asarray(I)
    hist = get_hist(src)

    new_src = to_gray(src)

    new = Image.fromarray(new_src)
    new.save(saves_path + 'new_3.png')

    fig, ax = plt.subplots()
    x = np.arange(0, 256)

    ax.plot(x, hist[0], color='r', label='histogram red')
    ax.plot(x, hist[1], color='g', label='histogram green')
    ax.plot(x, hist[2], color='b', label='histogram blue')

    plt.xlabel('Intensidad')
    fig.legend()


def cuatro():
    templates = []
    backgrounds = []
    foreground = asarray(Image.open(imgs_path + "fig_00.jpg"))
    for i in range(1, 5):
        templates.append(
            to_binary_region2(
                asarray(
                    Image.open(imgs_path + "pla_0%d.jpg" % i)
                )
            )
        )
        backgrounds.append(asarray(Image.open(imgs_path + "fig_0%d.jpg" % i)))

    templates = np.array(templates)
    backgrounds = np.array(backgrounds)

    new_images = []

    for i in range(4):
        new_images.append(
            merge_images(
                foreground, backgrounds[i], templates[i]))

    for i in range(4):
        img = Image.fromarray(new_images[i])
        img.save(saves_path + f"item4_0{i+1}.jpg")


def cinco(I: Image):
    src = asarray(I)
    alto, ancho, _ = src.shape

    r = src[:, :, 0]
    g = src[:, :, 1]
    b = src[:, :, 2]

    areas = {
        'red': m(r, 0, 0),
        'green': m(g, 0, 0),
        'blue': m(b, 0, 0),
    }

    print("Item 5:")
    for color in areas:
        print(f"  El area del color {color} es: {areas[color]}")


def seis(I: Image):
    src = asarray(I)
    gray_src = to_gray(src)

    hist = get_hist(src)
    gray_hist = get_hist(gray_src)

    r = hist[0]
    g = hist[1]
    b = hist[2]
    gray = gray_hist[0]
    x = np.arange(0, 256)

    fig, ax = plt.subplots()

    ax.plot(x, r, color='r', label='Histograma rojo')
    ax.plot(x, g, color='g', label='Histograma verde')
    ax.plot(x, b, color='b', label='Histograma azul')
    ax.plot(x, gray, color='gray', label='Histograma gris')

    valores = {
        'red': np.argmax(r),
        'green': np.argmax(g),
        'blue': np.argmax(b),
        'gray': np.argmax(gray)
    }

    ax.axhline(r[valores['red']], color='r', linestyle='--')
    ax.axhline(g[valores['green']], color='g', linestyle='--')
    ax.axhline(b[valores['blue']], color='b', linestyle='--')
    ax.axhline(gray[valores['gray']], color='gray', linestyle='--')

    ax.text(valores['red'], r[valores['red']], str(valores['red']))
    ax.text(valores['green'], g[valores['green']], str(valores['green']))
    ax.text(valores['blue'], b[valores['blue']], str(valores['blue']))
    ax.text(valores['gray'], gray[valores['gray']], str(valores['gray']))

    plt.xlabel('Intensidad')
    ax.legend()


def siete(I: Image):
    new_I = ImageOps.colorize(I, black="blue", white="white")
    new_I.save(saves_path + f"colorized_img.jpg")


def main():
    imgs = load_imgs(imgs_path)
    print('Las Imagenes del ítem 1 fueron transformadas a regiones binarias.')
    uno_a(imgs[0])
    uno_b(imgs[1])
    uno_c(imgs[2])
    dos(imgs[10])
    tres(imgs[9])
    cuatro()
    cinco(imgs[8])
    seis(imgs[3])
    siete(imgs[-1])

    graphics()


if __name__ == '__main__':
    main()
