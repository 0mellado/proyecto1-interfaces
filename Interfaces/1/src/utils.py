from PIL import Image, ImageOps
import matplotlib.pyplot as plt
import numpy as np
from os import listdir


def load_imgs(path: str) -> list:

    imgs = []
    for img in sorted(listdir(path)):
        imgs.append(Image.open(path + img))

    return imgs


def graphics():
    plt.show()


def to_binary_region1(I: list) -> list:
    new_src = np.zeros((len(I), len(I[0])), np.uint8)

    for v in range(len(I)):
        for u in range(len(I[v])):
            if I[v][u][0] == I[v][u][1] == I[v][u][2]:
                continue
            new_src[v][u] = 1

    return new_src

def to_binary_region2(I: list) -> list:
    new_src = np.zeros((len(I), len(I[0])), np.uint8)

    for v in range(len(I)):
        for u in range(len(I[v])):
            if I[v][u][0] == 255:
                continue
            new_src[v][u] = 1

    return new_src



def m(I: list, p: int, q: int) -> float:
    mpq = 0
    for v in range(len(I)):
        for u in range(len(I[v])):
            if (I[v][u] == 0):
                continue
            mpq += (u**p) * (v**q)

    return mpq

def to_gray(I: list):
    new_I = np.zeros((len(I), len(I[0]), 3), np.uint8)
    for v in range(len(I)):
        for u in range(len(I[v])):
            value = int(
                (int(I[v][u][0]) + int(I[v][u][1]) + int(I[v][u][2])) / 3)
            new_I[v][u] = np.array([value, value, value])

    return new_I

def centroide(I: list) -> tuple:
    m00 = m(I, 0, 0)
    return (m(I, 1, 0) / m00, m(I, 0, 1) / m00)

def u(I: list, p: int, q: int) -> float:
    x = centroide(I)
    upq = 0
    for v in range(len(I)):
        for u in range(len(I[v])):
            if (I[v][u] == 0):
                continue
            upq += ((u - x[0])**p) * ((v - x[1])**q)

    return upq


def u_n(I: list, p: int, q: int) -> float:
    return u(I, p, q) * ((1 / u(I, 0, 0)) ** ((p + q + 2) / 2))


def Hu_1(I: list) -> float:
    return (u_n(I, 2, 0) + u_n(I, 0, 2))


def Hu_2(I: list) -> float:
    return (((u_n(I, 2, 0) - u_n(I, 0, 2))**2) + 4 * (u_n(I, 1, 1)**2))


def Hu_3(I: list) -> float:
    return (((u_n(I, 3, 0) - (3 * u_n(I, 1, 2)))**2) + (((3 * u_n(I, 2, 1)) - u_n(I, 0, 3))**2))

def marca_centroide(I: Image, centroide: tuple):
    u = int(centroide[0]) - 4
    v = int(centroide[1]) - 4

    while u < centroide[0] + 4:
        I.putpixel((u, int(centroide[1])), (0, 0, 0))
        u += 1

    while v < centroide[1] + 4:
        I.putpixel((int(centroide[0]), v), (0, 0, 0))
        v += 1


def get_hist(I: list):
    hist = np.zeros((3, 256), np.uint64)

    for v in range(len(I)):
        for u in range(len(I[v])):
            hist[0][I[v][u][0]] += 1
            hist[1][I[v][u][1]] += 1
            hist[2][I[v][u][2]] += 1

    return hist

def merge_images(foreground: list, background: list, template: list) -> list:
    shape = (len(template), len(template[0]), len(background[0][0]))

    new_image = np.zeros(shape, np.uint8)

    for v in range(len(template)):
        for u in range(len(template[v])):
            if (template[v][u]):
                new_image[v][u] = background[v][u]
            else:
                new_image[v][u] = foreground[v][u]

    return new_image
