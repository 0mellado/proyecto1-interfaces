unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  ComCtrls, LazSerial, TAGraph, TASeries, TAChartCombos, fphttpclient,
  opensslsockets, fpjson, jsonparser;

type

  { TForm1 }

  TForm1 = class(TForm)
    Chart1: TChart;
    Chart2: TChart;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Serial: TLazSerial;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    Label1: TLabel;
    Label2: TLabel;
    Timer1: TTimer;

    procedure Chart1BeforeDrawBackWall(ASender: TChart; ACanvas: TCanvas;
      const ARect: TRect; var ADoDefaultDrawing: Boolean);
    procedure Chart2BeforeDrawBackWall(ASender: TChart; ACanvas: TCanvas;
      const ARect: TRect; var ADoDefaultDrawing: Boolean);

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SetTime(Sender: TObject);
    procedure SendHttpData(Sender: TObject);

  private
  public
  end;

var
  Form1: TForm1;
  Img_0: TPicture;
  Img_1: TPicture;


implementation

{$R *.lfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  Img_0 := TPicture.Create;
  Img_1 := TPicture.Create;

  Img_0.LoadFromFile('img/bkg1.png');
  Img_1.LoadFromFile('img/bkg2.png');

  Timer1.Enabled := True;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  Serial.Active := False;
  Serial.Close;
end;

procedure TForm1.Chart1BeforeDrawBackWall(Asender: TChart; ACanvas: TCanvas;
  const ARect: TRect; var ADoDefaultDrawing: boolean);
begin
  ACanvas.StretchDraw(ARect, Img_0.Graphic);
  ADoDefaultDrawing := False;
end;

procedure TForm1.Chart2BeforeDrawBackWall(ASender: TChart; ACanvas: TCanvas;
  const ARect: TRect; var ADoDefaultDrawing: Boolean);
begin
  ACanvas.StretchDraw(ARect, Img_1.Graphic);
  ADoDefaultDrawing := False;
end;

{Esto funciona con el componente TTimer y su evento OnTimer}

procedure TForm1.SetTime(Sender: TObject);
begin
  Label1.Caption := 'Fecha: ' + FormatDateTime('dd/mm/yyyy', Now) +
      ' - Hora: ' + FormatDateTime('hh:nn:ss', Now);
end;

{http funcional usar postJson.Add para añádir la data del MP}
{Se ejecuta el post al clickear el checkbox "Envía" }

procedure TForm1.SendHttpData(Sender: TObject);
var
  postJson: TJSONObject;
  responseBody: string;
begin
  if CheckBox2.Checked then
  begin
    postJson := TJSONObject.Create;
    postJson.Add('hello', 'hello from lazarus');

    with TFPHttpClient.Create(nil) do
      try
        AddHeader('Content-Type', 'application/json');
        RequestBody := TStringStream.Create(postJson.AsJSON);
        responseBody := Post('http://localhost:3000/data');
        Label2.Caption := responseBody;
      finally
        Free;
      end;
  end;
end;

end.

