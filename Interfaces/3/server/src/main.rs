use crate::model::DataModel;

use actix_web::{post, App, HttpResponse, HttpServer, Responder};

#[post("/data")]
async fn get_data() -> impl Responder {
    println!("alguien hizo req a esta ruta");
    HttpResponse::Ok().body("Hola esto es una prueba")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("Iniciando prototipo de servidor que recepciona datos de material particulado");
    HttpServer::new(|| App::new().service(get_data))
        .bind(("0.0.0.0", 7070))?
        .run()
        .await
}
