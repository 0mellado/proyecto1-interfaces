
#include <Avr/interrupt.h>
#include <avr/io.h>
#include <xc.h>

void my_setup() {
    cli();          // Limpiar interrupciones
    DDRB |= 0x01;   // Configurar PB0 como salida
    TCCR0A |= 0x00; // Configuración default
    TCCR0B |= 0x02; // Preescalador desactivado
    TIMSK0 |= 0x01; // Habilitar la interrupción por overflow
    TCNT0 = 226;    // Asegurarse de inicializar el timer/counter en 226
    sei();          // Habilitar interrupciones globales
}

ISR(TIMER0_OVF_vect) {
    PORTB ^= (1 << PB0); // Hacer un toggle al estado de PB0
    TCNT0 = 226;         // Asegurarse de inicializar el timer/counter en 226
}

int main(void) {

    my_setup();

    while (1) {
    }
}
