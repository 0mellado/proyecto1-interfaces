
#include <avr/interrupt.h>
#include <avr/io.h>
#include <xc.h>

/* Esta función se utiliza para configurar de forma inicial lo registros del micro */

void my_setup() {
    cli();         // esta funci�n se llama para limpiar las interrupciones del micro
    DDRB = 0x03;   // PB0 y PB1 como salidas, el resto como entradas
    DDRD = 0x00;   // todos los puertos D como entradas
    PORTB = 0x01;  // el pin PB0 parte en alto
    TCCR0A = 0x00; // los resultado de los comparadores no son salidas de los pines PD5 y PD6, y no se utilizar� el generador de frecuencias
    TCCR0B = 0x06; // se utiliza un clock externo en por el pin T0 en flanco de bajada, sin preescalador
    TIMSK0 = 0x01; // se activa la interrupci�n por desbordamiento
    TCNT0 = 0xfb;  // se configura el counter en 251 para que se desborde en 5 ciclos más
    sei();         // se activan las interrupciones globales
}

/* Esta función es la interrupción que se ejecutar cuando ocurra un overflow en el registro TCNT0 */

ISR(TIMER0_OVF_vect) {
    PORTB |= 0x02;     // se configura en alto el pin PB1 para reiniciar el contador externo
    TCNT0 = 0xfb;      // se devuelve al valor inicial el contador del micro
    PORTB ^= 1;        // se hace un toogle al pin PB0
    PORTB ^= (1 << 1); // se hace un toogle al pin PB1 para que vuelva a 0
}

int main(void) {
    my_setup(); // se configuran los registros del micro
    while (1) {
    };
}
